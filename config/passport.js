// It is undoubtly true that Javascript web token is more efficient than cookie.
// Here are some discussions & explansions 
// https://stackoverflow.com/questions/37582444/jwt-vs-cookies-for-token-based-authentication 
// https://stackoverflow.com/questions/31690427/how-are-cookies-different-from-jwt-and-why-are-they-considered-worse-than-jwt


const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt; 
const mongoose = require('mongoose');
const User = mongoose.model('user');
const keys = require('../config/keys');


const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = keys.secretOrKey;

module.exports = passport => {
    passport.use(
        new JwtStrategy(opts, (jwt_payload, done) => {
            User.findById(jwt_payload.id)
            .then(user => {
                if(user){
                    return done(null, user);
                }
                return done(null, false);
            })
            .catch(err => console.log(err));
    })
 );
};
