import React from 'react'
import Loader from './loader.gif';

export default () => {
  return (
    <div>
      <img src={Loader}
      style={{ width: '150px', margin: 'auto', display: 'block'}}
      alt="Loading"/>
    </div>
  )
}

