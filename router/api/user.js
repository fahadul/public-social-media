const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const passport = require('passport');

//Import input validation
const validateResisterInput = require ('../../validation/register');
const validateLoginInput = require ('../../validation/login');


//requiring User Schema from User in models

const User = require('../../models/User');

//route           GET api/user/test
//description     ....user route
//access          public route


//response to the 'get' request routed from the router by the declared variable name 

router.get('/test',(req, res) => res.json({msg:"user works"}));

//route           POST api/user/register
//description     ....register route
//access          public route


//response to the 'post' request routed from the router by the declared variable name

router.post('/register', (req, res) => {
        const {errors, isValid} = validateResisterInput(req.body);

        //Check validity
        if(!isValid){
            return res.status(400).json(errors);
        }

        User.findOne({email: req.body.email}) //find the email from the form
        .then(user =>{
            if(user){
                errors.email = 'Email already exists';
                return res.status(400).json(errors); //response if the user already exist
            } else {
                const avatar = gravatar.url(req.body.email, { //find the user in garvatar database, if the user email has a gravatar account fetch detail
                    s: '200', //Avatar size
                    r: 'pg', //rating
                    d: 'mm' //default
                });


                const newUser = new User({ // creting post request for user deatails
                    name: req.body.name,
                    email: req.body.email,
                    avatar,
                    password: req.body.password
                });

                bcrypt.genSalt(10, (err, salt) =>{      //using bycript to generate salt 
                    bcrypt.hash(newUser.password, salt, (err, hash) => { //hashing password using generated salt,
                        if (err) throw err;
                        newUser.password = hash;                          // using hashed password 
                        newUser.save()                                    // to save as new users password
                        .then(user => res.json(user))
                        .catch(err => console.log(err));
                    })
                })
            }
        })
});

//route           GET api/user/login
//description     Login user/ returning json web token
//access          public route
router.post('/login', (req, res) => {
    const {errors, isValid} = validateLoginInput(req.body);

        //Check validity
        if(!isValid){
            return res.status(400).json(errors);
        }

    const email = req.body.email;
    const password = req.body.password;

    //find user by email in database
    User.findOne({email})
    .then(user =>{
        if(!user){
            errors.email = 'User not found';
            return res.status(404).json(errors);
        }

        //Check password
        bcrypt.compare(password, user.password)
        .then(isMatch =>{
            if(isMatch){
                //usermatched
                const payload = { id: user.id, name: user.name, avatar: user.avatar }; // Creat jwt token

                //sign token
                //https://github.com/auth0/node-jsonwebtoken
                jwt.sign(payload, keys.secretOrKey, {expiresIn: '10h'}, (err, token) =>{
                    res.json({
                        success: true,
                        token: 'Bearer ' + token
                    });
                });
            } else{
                errors.password = 'password does not matches';
                return res.status(400).json(errors);
            }
        })
    });

});

//route           GET api/user/logged
//description     return logged user
//access          privete route

router.get('/logged', passport.authenticate('jwt', {session: false}), (req, res) =>{
    res.json({
        id: req.user.id,
        name: req.user.name,
        email: req.user.email
    });
});

module.exports = router;
