import React, { Component } from 'react'
import {Link, withRouter} from 'react-router-dom';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {addExperience} from '../../action/profileAction';


class AddExperience extends Component {
    constructor(props){
        super(props);
        this.state = {
            company: '',
            title: '',
            joblocation: '',
            from: '',
            to: '',
            current: false,
            jobdescription: '',
            errors: {},
            disabled: false 
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onCheck = this.onCheck.bind(this);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.errors){
            this.setState({errors: nextProps.errors});
        }
    }

    onSubmit(e){
        e.preventDefault();
        
        const expData = {
            company: this.state.company,
            title: this.state.title,
            joblocation: this.state.joblocation,
            from: this.state.from,
            to: this.state.to,
            current: this.state.current,
            jobdescription: this.state.jobdescription
        };
        
        this.props.addExperience(expData, this.props.history);
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value });
    }

    onCheck(e){
        this.setState({
            disabled: !this.state.disabled,
            current: !this.state.current
        });
    }

  render() {
      const {errors} = this.state;
    return (
      <div className="add-experience">
        <div className="container">
        <div className="row">
        <div className="col-md-8 m-auto">
        <Link to="/dashboard" className="btn btn-light">Back</Link>
        <h1 className="display-4 text-center">Add Experience</h1>
        <p className="lead text-center">Add your experiences</p>
        <small className="d-block pb-3">* = Required Field</small>
        <form onSubmit={this.onSubmit}>
            <TextFieldGroup
            placeholder = "* Company"
            name = "company"
            value = {this.state.company}
            onChange = {this.onChange}
            error = {errors.company}
            />

             <TextFieldGroup
            placeholder = "* Job Title"
            name = "title"
            value = {this.state.title}
            onChange = {this.onChange}
            error = {errors.title}
            />
             <TextFieldGroup
            placeholder = "* Job Location"
            name = "joblocation"
            value = {this.state.joblocation}
            onChange = {this.onChange}
            error = {errors.joblocation}
            />
            <h6>From Date</h6>
            <TextFieldGroup
            name = "from"
            type = "date"
            value = {this.state.from}
            onChange = {this.onChange}
            error = {errors.from}
            />
            <h6>To Date</h6>
            <TextFieldGroup
            name = "to"
            type="date"
            value = {this.state.to}
            onChange = {this.onChange}
            error = {errors.to}
            disabled= {this.state.disabled ? 'disabled' : ''}
            />
            <div className="form-check mb-4">
            <input type="checkbox" name="current" id="current" className="form-check-input" 
            value ={this.state.current} checked ={this.state.current} onChange={this.onCheck} />
            <label htmlFor="current" className="form-check-label">Current Job</label>
            </div>
            <TextAreaFieldGroup
            placeholder = "*Job Description"
            name = "jobdescription"
            value = {this.state.jobdescription}
            onChange = {this.onChange}
            error = {errors.jobdescription}
            info = "Write about your responsibilities"
            />
            <input type="submit" value="Submit" className="btn btn-info btn-block mt-4"/>
        </form>
        </div>
        </div>
        </div>
      </div>
    )
  }
}
AddExperience.proptypes ={
    profile: PropTypes.object.isRequired,
    addExperience: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    profile: state.profile,
    errors: state.errors
});

export default connect(mapStateToProps,{addExperience})(withRouter(AddExperience));
