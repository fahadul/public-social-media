import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCurrentProfile, deleteAccount } from '../../action/profileAction';
import Loader from '../common/Loader';
import {Link} from 'react-router-dom';
import ProfileAction from './ProfileAction';
import Experience from './Experience';
import Education from './Education';

class Dashboard extends Component {
    componentDidMount(){
        this.props.getCurrentProfile();
    }

    onDeleteClick(e){
        this.props.deleteAccount();
    }

  render() {
    const {user} = this.props.auth;
    const {profile, loading} = this.props.profile;

    let dashboardContent;

    if(profile === null || loading ){
        dashboardContent = < Loader />
    } else {
        //check logged users have profile date
        if(Object.keys(profile).length > 0){
            dashboardContent = (
                <div>
                    <p className="lead text-muted"> Welcome <Link to={`/profile/${profile.handle}`} >{user.name}</Link> </p>
                    <ProfileAction />
                    <Experience experience={profile.experience}/>
                    <Education education={profile.education}/>
                    <div style={{marginBottom: '80px'}} >
                        <button onClick={this.onDeleteClick.bind(this)} className="btn btn-danger">Delete My Account</button>
                    </div>
                </div>
            );
        } else {
            // User is logged but has no profile data
            dashboardContent = (
                <div>
                    <p className="lead text-muted"> Welcome {user.name}</p>
                    <p>Please add some info to your profile</p>
                    <Link to="/create-profile" className="btn btn-lg btn-info">Creat Profile</Link>
                </div>
            );
        }
    }


    return (
      <div className="dashboard">
        <div className="container">
        <div className="row">
        <div className="col-md-12">
        <h1 className="display-4">
        Dashboard
        {dashboardContent}
        </h1>
        </div>
        </div>
        </div>
      </div>
    )
  }
}

Dashboard.propTypes = {
    getCurrentProfile: PropTypes.func.isRequired,
    deleteAccount: PropTypes.func.isRequired,
    profile: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    profile: state.profile,
    auth: state.auth
});

export default connect(mapStateToProps, {getCurrentProfile, deleteAccount})(Dashboard);
