import axios from 'axios';
import {GET_PROFILE, GET_PROFILES, PROFILE_LOADING, CLEAR_CURRENT_PROFILE, SET_CURRENT_USER, GET_ERRORS } from './types';

//get current profile
export const getCurrentProfile = () => dispatch =>{
    dispatch(setProfileLoading());
    axios.get('/api/profile')
    .then(res => dispatch({
        type: GET_PROFILE,
        payload: res.data
    }))
    .catch(err => dispatch({
        type: GET_PROFILE,
        payload: {}
    }));
}
//create profile
export const createProfile = (profileData, history) => dispatch =>{
    axios
    .post('/api/profile', profileData)
    .then(res => history.push('/dashboard'))
    .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
    }));
};

//add experience
export const addExperience = (expData, history) => dispatch => {
    axios
    .post('/api/profile/experience', expData)
    .then(res => history.push('/dashboard'))
    .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
    }));
};
//add education
export const addEducation = (eduData, history) => dispatch => {
    axios
    .post('/api/profile/education', eduData)
    .then( res => history.push('/dashboard'))
    .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
    }));
};

//deleteExperience
export const deleteExperience = id => dispatch => {
    axios
    .delete(`/api/profile/experience/${id}`)
    .then(res => dispatch({
        type: GET_PROFILE,
        payload: res.data
    }))
    .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
    }));
};
//get all profile
export const getProfiles = () => dispatch => {
    dispatch(setProfileLoading());
    axios
    .get('/api/profile/all')
    .then(res => dispatch({
        type: GET_PROFILES,
        payload: res.data
    }))
    .catch(err => dispatch({
        type: GET_PROFILES,
        payload: null
    }));
};

//get profile by handle
export const getProfileByHandle = (handle) => dispatch =>{
    dispatch(setProfileLoading());
    axios
    .get(`/api/profile/handle/${handle}`)
    .then(res => dispatch({
        type: GET_PROFILE,
        payload: res.data
    }))
    .catch(err => dispatch({
        type: GET_PROFILE,
        payload: null
    }));
}

//deleteExperience
export const deleteEducation = id => dispatch => {
    axios
    .delete(`/api/profile/education/${id}`)
    .then(res => dispatch({
        type: GET_PROFILE,
        payload: res.data
    }))
    .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
    }));
};

//delete account 
export const deleteAccount = () => dispatch => {
    if(window.confirm('This can not be undone anymore. Are you sure you want to DELETE your account ?')){
        axios
        .delete('/api/profile')
        .then(res => dispatch({
            type: SET_CURRENT_USER,
            payload: {}
        }))
        .catch(err => dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        }));
    }
};


export const setProfileLoading = () => {
    return {
        type: PROFILE_LOADING
    };
};

//clear profile when logged out
export const clearCurrentProfile = () => {
    return {
        type: CLEAR_CURRENT_PROFILE
    };
};


//https://blog.rangle.io/creating-forms-with-redux-part-1/
//https://davidkpiano.github.io/react-redux-form/docs/api/actions.html