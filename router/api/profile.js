const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

//Validation
const validateProfileInput = require('../../validation/profile');
const validateExperienceInput = require('../../validation/experience');
const validateEducationInput = require('../../validation/education');

//Load profile model
const Profile = require('../../models/Profile');
//Load user profile
const User = require('../../models/User');

//#####route           GET api/profile/test
//####description     ....profile route
//####access          public route


//response to the 'get' request routed from the router by the declared variable name 
router.get('/test',(req, res) => res.json({msg:"profile works"}));



//#####route           GET api/profile
//####description     Get current user profile
//####access          private route
router.get('/', passport.authenticate('jwt', {session: false}), (req, res) =>{
    const errors = {};
    Profile.findOne({user: req.user.id})
    .populate('user',['name', 'avatar'])
    .then(profile =>{
        if(!profile){
            errors.noprofile = 'There is no profile for this user';
            return res.status(404).json(errors);
        }
        res.json(profile);
    })
    .catch(err => res.status(404).json(err));
});

//#####route           POST api/profile/handle/:handle
//####description      Get Profile By Handle
//####access          public route

router.get('/handle/:handle', (req, res) => {
   const errors = {}; 
   Profile.findOne({ handle: req.params.handle})
   .populate('user', ['name', 'avatar'])
   .then(profile => {
       if(!profile) {
           errors.noprofile = 'There is no profile for this user';
           res.status(404).json(errors);
       } 
       res.json(profile);
   })
   .catch(err => res.status(404).json(err));
});

//#####route           POST api/profile/user/:user_id
//####description      Get Profile By user_id
//####access          public route

router.get('/all', (req, res) => {
    const errors = {};
    Profile.find()
    .populate('user', ['name', 'avatar'])
    .then(profiles => {
        if(!profiles){
            errors.noprofile = 'No profile available';
            res.status(404).json(errors);
        }
        res.json(profiles);
    })
    .catch(err => res.status(404).json({profile: 'No profile available at this moment'}));
})

//#####route           POST api/profile/user/:user_id
//####description      Get Profile By user_id
//####access          public route

router.get('/user/:user_id', (req, res) => {
    const errors = {}; 
    Profile.findOne({ user: req.params.user_id})
    .populate('user ', ['name', 'avatar'])
    .then(profile => {
        if(!profile) {
            errors.noprofile = 'There is no profile for this user';
            res.status(404).json(errors);
        }
        res.json(profile);
    })
    .catch(err => res.status(404).json({profile: 'There is no profile with this ID'}));
 });

//#####route           POST api/profile
//####description      Creat or edit User Profile
//####access          private route
router.post('/', passport.authenticate('jwt', {session: false}), (req, res) =>{
    const {errors, isValid} = validateProfileInput(req.body);
    if(!isValid){
        return res.status(400).json(errors);
    }

    const profileFields = {};
    profileFields.user = req.user.id;
    if(req.body.handle) profileFields.handle = req.body.handle;
    if(req.body.company) profileFields.company = req.body.company;
    if(req.body.website) profileFields.website = req.body.website;
    if(req.body.location) profileFields.location = req.body.location;
    if(req.body.status) profileFields.status = req.body.status;
    if(req.body.bio) profileFields.bio = req.body.bio;
    if(req.body.githubusername) profileFields.githubusername = req.body.githubusername;

    //skills split into an array
    if(typeof req.body.skills !== 'undefined'){
        profileFields.skills = req.body.skills.split(',');
    }
    profileFields.social = {};
    if(req.body.youtube) profileFields.social.youtube = req.body.youtube;
    if(req.body.facebook) profileFields.social.facebook = req.body.facebook;
    if(req.body.twitter) profileFields.social.twitter = req.body.twitter;
    if(req.body.linkedin) profileFields.social.linkedin = req.body.linkedin;
    if(req.body.instagramm) profileFields.social.instagramm = req.body.instagramm;

    Profile.findOne({ user: req.user.id })
    .then(profile =>{
        if(profile){
            //Update
            Profile.findOneAndUpdate({user: req.user.id}, 
                {$set: profileFields}, 
                {new: true})
                .then(profile => res.json(profile));
        } else {
            // Create
            //Check handle
            Profile.findOne({handle: profileFields.handle})
            .then(profile =>{
                if(profile){
                    errors.handle = 'That handle already exist';
                    res.status(400).json(errors);
                }
                //Save
                new Profile(profileFields).save().then(profile => res.json(profile));
            });
        }
    });

});

//#####route           POST api/profile/experience
//####description      Add experience to profile
//####access           Private route

router.post('/experience', passport.authenticate('jwt',{ session: false}), (req, res) => {
    
    const {errors, isValid} = validateExperienceInput(req.body);
    if(!isValid){
        return res.status(400).json(errors);
    }

    Profile.findOne({user: req.user.id})
    .then(profile => {
        const newExp = {
            title: req.body.title,
            company: req.body.company,
            joblocation: req.body.joblocation,
            from: req.body.from,
            to: req.body.to,
            current: req.body.current,
            jobdescription: req.body.jobdescription
        }

        // Add to experience list
        profile.experience.unshift(newExp);
        profile.save().then(profile => res.json(profile));
    })
} );

//#####route           POST api/profile/education
//####description      Add educational data to profile
//####access           Private route

router.post('/education', passport.authenticate('jwt',{ session: false}), (req, res) => {
    
    const {errors, isValid} = validateEducationInput(req.body);
    if(!isValid){
        return res.status(400).json(errors);
    }

    Profile.findOne({user: req.user.id})
    .then(profile => {
        const newEdu = {
            school: req.body.school,
            faculty: req.body.faculty,
            degree: req.body.degree,
            location: req.body.location,
            from: req.body.from,
            to: req.body.to,
            current: req.body.current,
            description: req.body.description
        }

        // Add to education list
        profile.education.unshift(newEdu);
        profile.save().then(profile => res.json(profile));
    })
} );

//#####route           DELETE api/profile/experience
//####description      Delete experience data to profile
//####access           Private route

router.delete('/experience/:exp_id', passport.authenticate('jwt',{ session: false}), 
(req, res) => {
    
        Profile.findOne({user: req.user.id})
        .then(profile => {
            //get the item from array to remove
            const removeIndex = profile.experience
            .map(item => item.id)
            .indexOf(req.params.exp_id);
            
            // remove (splice out) from array
            profile.experience.splice(removeIndex, 1);

            //save
            profile.save().then(profile => res.json(profile));
        })
        .catch(err => res.status(404).json(err));
  }
);

//#####route           DELETE api/profile/education
//####description      Delete education data to profile
//####access           Private route

router.delete('/experience/:edu_id', passport.authenticate('jwt',{ session: false}), 
(req, res) => {
    
        Profile.findOne({user: req.user.id})
        .then(profile => {
            //get the item from array to remove
            const removeIndex = profile.education
            .map(item => item.id)
            .indexOf(req.params.edu_id);
            
            // remove (splice out) from array
            profile.education.splice(removeIndex, 1);

            //save
            profile.save().then(profile => res.json(profile));
        })
        .catch(err => res.status(404).json(err));
  }
);

//#####route           DELETE api/profile/
//####description      Delete user and profile
//####access           Private route

router.delete('/', passport.authenticate('jwt',{ session: false}), 
(req, res) => {
    
        Profile.findOneAndRemove({user: req.user.id})
        .then(() => {
            User.findOneAndRemove({ _id: req.user.id}).then(() => 
                res.json({ success: true})
        );
        });
  }
);

module.exports = router;
