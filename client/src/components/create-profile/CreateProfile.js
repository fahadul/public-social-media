import React, { Component } from 'react'
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextFieldGroup';
import SelectListGroup from '../common/SelectListGroup';
import InputGroup from '../common/InputGroup';
import {createProfile} from '../../action/profileAction';

class CreateProfile extends Component {
    constructor(props){
        super(props);
        this.state = {
            displaySocialInputs: false,
            handle: '',
            company: '',
            website: '',
            location: '',
            status: '',
            skills: '',
            githubusername: '',
            bio: '',
            twitter: '',
            facebook: '',
            linkedin: '',
            youtube: '',
            instagram: '',
            errors: {}
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.errors){
            this.setState({errors: nextProps.errors})
        }
    }

    onSubmit(e){
        e.preventDefault();
        
        const profileData = {
            handle: this.state.handle,
            company: this.state.company,
            website: this.state.website,
            location: this.state.location,
            status: this.state.status,
            skills: this.state.skills,
            githubusername: this.state.githubusername,
            bio: this.state.bio,
            twitter: this.state.twitter,
            facebook: this.state.facebook,
            instagram: this.state.instagram,
            youtube: this.state.youtube,
            linkedin: this.state.linkedin
        }

        this.props.createProfile(profileData, this.props.history);
    }
    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    render() {
        const {errors, displaySocialInputs} = this.state;
        let socialInputs;

        if(displaySocialInputs){
            socialInputs = (
                <div>
                    <InputGroup 
                    placeholder= "Twitter Profile URL"
                    name="twitter"
                    icon = "fab fa-twitter"
                    value={this.state.twitter}
                    onChange={this.onChange}
                    error={errors.twitter}
                    />
                    <InputGroup 
                    placeholder= "Facebook Profile URL"
                    name="facebook"
                    icon = "fab fa-facebook"
                    value={this.state.facebook}
                    onChange={this.onChange}
                    error={errors.facebook}
                    />
                    <InputGroup 
                    placeholder= "Youtube URL"
                    name="youtube"
                    icon = "fab fa-youtube"
                    value={this.state.youtube}
                    onChange={this.onChange}
                    error={errors.youtube}
                    />
                    <InputGroup 
                    placeholder= "Instagram Profile URL"
                    name="instagram"
                    icon = "fab fa-instagram"
                    value={this.state.instagram}
                    onChange={this.onChange}
                    error={errors.instagram}
                    />
                    <InputGroup 
                    placeholder= "Linkedin Profile URL"
                    name="linkedin"
                    icon = "fab fa-linkedin"
                    value={this.state.linkedin}
                    onChange={this.onChange}
                    error={errors.linkedin}
                    />
                </div>
            )
        }

        const options = [
           {label: '* Select Professional Status', value:0},
           {label: 'Executive', value:'Executive'},
           {label: 'Junior Executive', value:'Junior Executive'},
           {label: 'Manager', value:'Manager'},
           {label: 'Trainee', value:'Trainee'},
           {label: 'Instructor', value:'Instructor'},
           {label: 'Intern', value:'Intern'},
           {label: 'Developer', value:'Developer'},
           {label: 'Student', value:'Student'},
           {label: 'Other', value:'Other'}
        ];
    return (
      <div className="create-profile">
        <div className="container">
        <div className="row">
        <div className="col-md-8 m-auto">
        <h1 className="display-4 text-center">Create Profile</h1>
        <p className="lead text-center">
        Add some information to make your profile
        </p>
        <small className="d-block pb-3">* = required</small>
        <form onSubmit={this.onSubmit} >
            <TextFieldGroup 
            placeholder= "* Profile Handle"
            name="handle"
            value={this.state.handle}
            onChange={this.onChange}
            error={errors.handle}
            info="A unique handle for your profile URL, your full name, nickname, company name"
            />
            <SelectListGroup 
            placeholder= "Status"
            name="status"
            value={this.state.status}
            options={options}
            onChange={this.onChange}
            error={errors.status}
            info="Add your career"
            />
            <TextFieldGroup 
            placeholder= "Company Or Organization"
            name="company"
            value={this.state.company}
            onChange={this.onChange}
            error={errors.company}
            info="It could be your own organization or the organization you work with "
            />
            <TextFieldGroup 
            placeholder= "Website"
            name="website"
            value={this.state.website}
            onChange={this.onChange}
            error={errors.website}
            info="It could be your own website or the website you work with "
            />
            <TextFieldGroup 
            placeholder= "Location Or Address"
            name="location"
            value={this.state.location}
            onChange={this.onChange}
            error={errors.location}
            info="It could be your own location or the location where you work at "
            />
            <TextFieldGroup 
            placeholder= "Skills"
            name="skills"
            value={this.state.skills}
            onChange={this.onChange}
            error={errors.skills}
            info="Please use comma separated value (eg. Programming, Management, Development, Communication etc )"
            />
            <TextFieldGroup 
            placeholder= "Github Username"
            name="githubusername"
            value={this.state.githubusername}
            onChange={this.onChange}
            error={errors.githubusername}
            info="You may include github username "
            />
            <TextAreaFieldGroup 
            placeholder= "Bio"
            name="bio"
            value={this.state.bio}
            onChange={this.onChange}
            error={errors.bio}
            info="You may include github username "
            />
            <div className="mb-3">
            <button type="button" onClick={()=>{
                this.setState(prevState =>({
                    displaySocialInputs: !prevState.displaySocialInputs 
                }))
            }} className="btn btn-light">Add Social Network Link</button>
            <span className="text-muted">Optional</span>
            </div>
            {socialInputs}
            <input type="submit" value="Submit" className="btn btn-info btn-block mt-4"/>
                        
        </form>
        </div>
        </div>
        </div>
      </div>
    )
  }
}

CreateProfile.propTypes  ={
    profile: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    profile: state.profile,
    errors: state.errors
});

export default connect(mapStateToProps, {createProfile})(withRouter(CreateProfile));
