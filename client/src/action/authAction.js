//https://redux.js.org/basics/actions
import axios from 'axios';
import { GET_ERRORS, SET_CURRENT_USER } from './types';
import setAuthToken from '../utilities/setAuthToken';
import jwt_decode from 'jwt-decode';

//register user action
export const registerUser =(userData, history) => dispatch => {
    axios
    .post('/api/user/register', userData)
    .then(res => history.push('/login'))
    .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
    }));
};

//login user action
export const loginUser = (userData) => dispatch => {
    axios
    .post('/api/user/login', userData)
    .then(res =>{
        //save token in ls
        const {token} = res.data;
        //set stored token
        localStorage.setItem('jwtToken', token);
        //set token to auth header so it'll be available throughout the app
        setAuthToken(token);
        //decode data from token
        const decoded = jwt_decode(token);
        //set current user
        dispatch(setCurrentUser(decoded));
    })
    .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
    }));
};
//set signed user
export const setCurrentUser = (decoded) => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    }
};

//log out
export const logoutUser = () => dispatch => {
    //remove token
    localStorage.removeItem('jwtToken');
    //remove auth header
    setAuthToken(false);
    // set currentuser to an empty object that will make isAuthenticated false
    dispatch(setCurrentUser({}));
};