const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateProfileInput(data){
    let errors = {};
     
    
    data.handle = !isEmpty(data.handle) ? data.handle : ''; //Check the handle exist 
    data.status = !isEmpty(data.status) ? data.status : '';
    data.skills = !isEmpty(data.skills) ? data.skills : '';
    

    //Requiring handle length
   if (!Validator.isLength(data.handle,{min:2 , max: 40})) {
       errors.handle = 'Handle need to be between tow to fourty chracter';
   }
   //Requiring handle
   if (Validator.isEmpty(data.handle)) {
    errors.handle = 'Profile handle is required';
   }
   //Requiring status
   if (Validator.isEmpty(data.status)) {
    errors.status = 'Status is required';
   }
   //Requiring skills
   if (Validator.isEmpty(data.skills)) {
    errors.skills = 'Skill is required';
   }
   //Checking url format
   if (!isEmpty(data.website)) {
    if(!Validator.isURL(data.website)){
        errors.website = 'Website URL is not formated correctly';
    }
   }
   //Checking social media url
   if (!isEmpty(data.github)) {
    if(!Validator.isURL(data.github)){
        errors.github = 'Github URL is not formated correctly';
    }
   }

   if (!isEmpty(data.youtube)) {
    if(!Validator.isURL(data.youtube)){
        errors.youtube = 'Youtube URL is not formated correctly';
    }
   }

   if (!isEmpty(data.facebook)) {
    if(!Validator.isURL(data.facebook)){
        errors.facebook = 'Facebook URL is not formated correctly';
    }
   }

   if (!isEmpty(data.twitter)) {
    if(!Validator.isURL(data.twitter)){
        errors.twitter = 'Twitter URL is not formated correctly';
    }
   }

   if (!isEmpty(data.instagram)) {
    if(!Validator.isURL(data.instagram)){
        errors.instagram = 'Instagram URL is not formated correctly';
    }
   }

   if (!isEmpty(data.linkedin)) {
    if(!Validator.isURL(data.linkedin)){
        errors.linkedin = 'Linkedin URL is not formated correctly';
    }
   }

   
    

    return {
        errors,
        isValid: isEmpty(errors)
    }
}