import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import Loader from '../common/Loader';
import { getProfiles } from '../../action/profileAction';
import ProfileItem from './ProfileItem';

class Profiles extends Component {
    componentDidMount(){
        this.props.getProfiles();
    }

  render() {
      const {profiles, loading} = this.props.profile;
      let profileItems;

      if(profiles === null || loading){
          profileItems = <Loader />;
      } else {
          if(profiles.length > 0){
            profileItems= profiles.map(profile => (
                <ProfileItem key={profile._id} profile={profile} />
            ))
          } else {
              profileItems = <h5> No Profile Found ...</h5>
          }
      }
    return (
      <div className="profiles">
        <div className="container">
        <div className="row">
        <div className="col-md-12">
        <h2 className="display-4 text-center">Users Profiles</h2>
        <p className="lead text-center">Find other professionals</p>
        {profileItems}
        </div>
        </div>
        </div>
      </div>
    )
  }
}

Profiles.propTypes = {
    getProfiles: PropTypes.func.isRequired,
    profile: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    profile: state.profile
});

export default connect(mapStateToProps,{getProfiles})(Profiles);
