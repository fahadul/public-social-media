const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');
const passport = require('passport');

//requiring user, profile and post from their directory by declaring them variable

const post = require('./router/api/post');
const profile = require('./router/api/profile');
const user = require('./router/api/user');

//Declaring 'express' as 'app' variable
const app = express();

//Middleware of bodyParser
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//DB Config
const db = require('./config/keys').mongoURI;

//Connect to db
mongoose
    .connect(db)
    .then(() => console.log('Mongo is connected'))
    .catch(err => console.log(err));

//Passport Middleware
app.use(passport.initialize());

//Confuguring passport
require('./config/passport')(passport);

//Using router to route through 'api' folder by their name

app.use('/api/user', user);
app.use('/api/profile', profile);
app.use('/api/post', post);

//in production serve static 
if(process.env.NODE_ENV === 'production'){
    //set static folder
    app.use(express.static('client/build'));
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));

// "heroku-postbuild": "NPM_CONFIG_PRODUCTION=false npm install --prefix client && npm run build --prefix client"

