import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './store';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utilities/setAuthToken';
import { setCurrentUser, logoutUser } from './action/authAction';


import './App.css';
import PrivateRoute from './components/common/PrivateRoute';
import Navbar from './components/layout/Navbar';
import Footer from './components/layout/Footer';
import Landing from './components/layout/Landing';
import Register from './components/auth/Register';
import Login from './components/auth/Login';
import Dashboard from './components/dashboard/Dashboard';
import CreateProfile from './components/create-profile/CreateProfile';
import { clearCurrentProfile } from './action/profileAction';
import EditProfile from './components/edit/EditProfile';
import AddExperience from './components/add-exprience/AddExperience';
import AddEducation from './components/add-exprience/AddEducation';
import Profiles from './components/profiles/Profiles';
import Profile from './components/profile/Profile';
import Posts from './components/post/Posts';
import Post from './components/Post/Post';

if(localStorage.jwtToken){
  //set auth token header
  setAuthToken(localStorage.jwtToken);
  //decode token
  const decoded = jwt_decode(localStorage.jwtToken);
  // set user and auth
  store.dispatch(setCurrentUser(decoded));
  //check token expiration
  const currentTime = Date.now() / 1000;
  if(decoded.exp < currentTime){
    store.dispatch(logoutUser());
    store.dispatch(clearCurrentProfile());
    window.location.href = '/login';
  }
}



class App extends Component {
  render() {
    return (
      <Provider store={store}>
          <Router>
          <div className="App">
          <Navbar />
          <Route exact path="/" component={ Landing } />
          <div className="container">
          <Route exact path="/register" component={ Register } />
          <Route exact path="/login" component={ Login } />
          <Route exact path="/profiles" component={ Profiles } />
          <Route exact path="/profile/:handle" component={ Profile } />
          <Switch>
          <PrivateRoute exact path="/dashboard" component={ Dashboard } />
          </Switch>
          <Switch>
          <PrivateRoute exact path="/create-profile" component={ CreateProfile } />
          </Switch>
          <Switch>
          <PrivateRoute exact path="/edit" component={ EditProfile } />
          </Switch>
          <Switch>
          <PrivateRoute exact path="/add-experience" component={ AddExperience } />
          </Switch>
          <Switch>
          <PrivateRoute exact path="/add-education" component={ AddEducation } />
          </Switch>
          <Switch>
          <PrivateRoute exact path="/feed" component={ Posts } />
          </Switch>
          <Switch>
          <PrivateRoute exact path="/Post/:id" component={ Post } />
          </Switch>
          </div>
          <Footer />
          </div>
          </Router>
      </Provider>
    );
  }
}

export default App;
