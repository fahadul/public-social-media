import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../action/authAction';
import { clearCurrentProfile } from '../../action/profileAction';

class Navbar extends Component {
  onLogoutClick(e){
    e.preventDefault();
    this.props.clearCurrentProfile();
    this.props.logoutUser();
  }
  render() {
    const { isAuthenticated, user } = this.props.auth;

    //this link will appear for logged user on navbar
    const authLink = (
      <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/feed">Posts</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/dashboard">Dashboard</Link>
              </li>
              <li className="nav-item">
              <a href="" onClick={this.onLogoutClick.bind(this)} className="nav-link">
              <img className="rounded-circle" src={user.avatar} alt={user.name} style={{ width: '26px', marginRight: '5px'}} title="Currently this app only supports gravatar, only gravatar enabled users will see their profle picture but soon users will be able to upload image"/>
              {' '}
              Logout
              </a>
              </li>
            </ul>
    );
    //this link will appear for guest user on navbar
    const guestLink = (
      <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/register">Register</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/login">Login</Link>
              </li>
            </ul>
    );

    return (
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-4">
        <div className="container">
          <Link className="navbar-brand" to="/">SocialHub</Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile-nav">
            <span className="navbar-toggler-icon"></span>
          </button>
    
          <div className="collapse navbar-collapse" id="mobile-nav">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/profiles"> Professionals
                </Link>
              </li>
            </ul>
    
             { isAuthenticated ? authLink : guestLink }
          </div>
        </div>
      </nav>
    )
  }
}

Navbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth:PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth
});

export default  connect(mapStateToProps, { logoutUser, clearCurrentProfile })(Navbar);
