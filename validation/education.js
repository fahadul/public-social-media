const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateEducationInput(data){
    let errors = {};

    
    data.school = !isEmpty(data.school) ? data.school : '';
    data.faculty = !isEmpty(data.faculty) ? data.faculty : '';
    data.degree = !isEmpty(data.degree) ? data.degree : '';
    data.from = !isEmpty(data.from) ? data.from : '';
    

    
    
   

    if(Validator.isEmpty(data.school)){
        errors.school = 'School field is required';
    }
    if(Validator.isEmpty(data.faculty)){
        errors.faculty = 'User must add a faculty';
    }
    if(Validator.isEmpty(data.degree)){
        errors.degree = 'User must add a degree';
    }
    if(Validator.isEmpty(data.from)){
        errors.from = 'From date field is required';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}