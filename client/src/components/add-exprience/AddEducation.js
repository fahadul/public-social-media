import React, { Component } from 'react'
import {Link, withRouter} from 'react-router-dom';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {addEducation} from '../../action/profileAction';


class AddEducation extends Component {
    constructor(props){
        super(props);
        this.state = {
            school: '',
            degree: '',
            faculty: '',
            from: '',
            to: '',
            current: false,
            description: '',
            errors: {},
            disabled: false 
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onCheck = this.onCheck.bind(this);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.errors){
            this.setState({errors: nextProps.errors});
        }
    }

    onSubmit(e){
        e.preventDefault();
        
        const eduData = {
            school: this.state.school,
            degree: this.state.degree,
            faculty: this.state.faculty,
            from: this.state.from,
            to: this.state.to,
            current: this.state.current,
            description: this.state.description
        };
        
        this.props.addEducation(eduData, this.props.history);
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value });
    }

    onCheck(e){
        this.setState({
            disabled: !this.state.disabled,
            current: !this.state.current
        });
    }

  render() {
      const {errors} = this.state;
    return (
      <div className="add-education">
        <div className="container">
        <div className="row">
        <div className="col-md-8 m-auto">
        <Link to="/dashboard" className="btn btn-light">Back</Link>
        <h1 className="display-4 text-center">Add Education</h1>
        <p className="lead text-center">Add your educational details</p>
        <small className="d-block pb-3">* = Required Field</small>
        <form onSubmit={this.onSubmit}>
            <TextFieldGroup
            placeholder = "* School/Institution/Uni"
            name = "school"
            value = {this.state.school}
            onChange = {this.onChange}
            error = {errors.school}
            />

             <TextFieldGroup
            placeholder = "* Name Of Degree"
            name = "degree"
            value = {this.state.degree}
            onChange = {this.onChange}
            error = {errors.degree}
            />
             <TextFieldGroup
            placeholder = "* Faculty"
            name = "faculty"
            value = {this.state.faculty}
            onChange = {this.onChange}
            error = {errors.faculty}
            />
            <h6>From Date</h6>
            <TextFieldGroup
            name = "from"
            type = "date"
            value = {this.state.from}
            onChange = {this.onChange}
            error = {errors.from}
            />
            <h6>To Date</h6>
            <TextFieldGroup
            name = "to"
            type="date"
            value = {this.state.to}
            onChange = {this.onChange}
            error = {errors.to}
            disabled= {this.state.disabled ? 'disabled' : ''}
            />
            <div className="form-check mb-4">
            <input type="checkbox" name="current" id="current" className="form-check-input" 
            value ={this.state.current} checked ={this.state.current} onChange={this.onCheck} />
            <label htmlFor="current" className="form-check-label">Currently I Study Here</label>
            </div>
            <TextAreaFieldGroup
            placeholder = "Description about subject/faculty/uni"
            name = "description"
            value = {this.state.description}
            onChange = {this.onChange}
            error = {errors.description}
            info = "Write about your program details"
            />
            <input type="submit" value="Submit" className="btn btn-info btn-block mt-4"/>
        </form>
        </div>
        </div>
        </div>
      </div>
    )
  }
}

AddEducation.proptypes ={
    profile: PropTypes.object.isRequired,
    addEducation: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    profile: state.profile,
    errors: state.errors
});

export default connect(mapStateToProps, {addEducation})(withRouter(AddEducation));
