const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const Post = require('../../models/Post');
const Profile = require('../../models/Profile');
const validatePostInput = require('../../validation/post');

//route           GET api/post/test
//description     ....post route
//access          public route


//response to the 'get' request routed from the router by the declared variable name 

router.get('/test',(req, res) => res.json({msg:"post works"}));

//route           GET api/post
//description     get post route
//access          public route
router.get('/', (req, res) =>{
    Post.find()
    .sort({date: -1})
    .then(posts => res.json(posts))
    .catch(err => res.status(404));
});

//route           GET api/post/:id
//description     get post by id
//access          public route
router.get('/:id', (req, res) =>{
    Post.findById(req.params.id)
    .then(post => res.json(post))
    .catch(err => res.status(404).json({noPostFound: 'No post found with that id'}));
});

//route           POST api/post
//description     create post
//access          private route
router.post('/', passport.authenticate('jwt', {session: false}), (req, res) => {
    const {errors, isValid} = validatePostInput(req.body);
    if(!isValid){
        return res.status(400).json(errors);
    }

    const newPost = new Post({
        text: req.body.text,
        name: req.body.name,
        avatar: req.body.avatar,
        user: req.user.id
    });
    newPost.save().then(post => res.json(post));
});

//route           DELETE api/post/:id
//description     create post
//access          private route
router.delete('/:id', passport.authenticate('jwt', {session: false}),(req, res) =>{
    Profile.findOne({user: req.user.id})
    .then(profile =>{
        Post.findById(req.params.id)
        .then(post =>{
            //check the owner
            if(post.user.toString() !== req.user.id){
                return res.status(401).json({notAuthorized: 'Unauthorized action'});
            }
            //remove
            post.remove().then(() => res.json({success: true}))
        })
        .catch(err => res.status(404).json({noPostFound: 'No post found with that id'}));
    })
});

//route           POST api/post/like/:id
//description     like post
//access          private route
router.post('/like/:id', passport.authenticate('jwt', {session: false}),(req, res) =>{
    Profile.findOne({user: req.user.id})
    .then(profile =>{
        Post.findById(req.params.id)
        .then(post =>{
           if(post.likes.filter(like =>like.user.toString() === req.user.id).length >0){
               return res.status(400).json({alreadyLiked: 'User already liked'})
           }

           //Add user id to like list
           post.likes.unshift({user: req.user.id});

           //save likes
           post.save().then(post => res.json(post));
        })
        .catch(err => res.status(404).json({noPostFound: 'No post found with that id'}));
    })
});

//route           POST api/post/unlike/:id
//description     like post
//access          private route
router.post('/unlike/:id', passport.authenticate('jwt', {session: false}),(req, res) =>{
    Profile.findOne({user: req.user.id})
    .then(profile =>{
        Post.findById(req.params.id)
        .then(post =>{
           if(post.likes.filter(like =>like.user.toString() === req.user.id).length === 0){
               return res.status(400).json({notLiked: 'You have not liked yet'})
           }

           //Get remove index
           const removeIndex = post.likes
           .map(item => item.user.toString())
           .indexOf(req.user.id);

           //splice out
           post.likes.splice(removeIndex, 1);

           //save
           post.save().then(post => res.json(post));
           
        })
        .catch(err => res.status(404).json({noPostFound: 'No post found with that id'}));
    })
});

//route           POST api/post/comment/:id
//description     comment post
//access          private route
router.post('/comment/:id', passport.authenticate('jwt', {session: false}), (req, res) =>{
    //we have used same validation on comment like we did on post
    const {errors, isValid} = validatePostInput(req.body);
    if(!isValid){
        return res.status(400).json(errors);
    }

    Post.findById(req.params.id)
    .then(post => {
        const newComment = {
            text: req.body.text,
            name: req.body.name,
            avatar: req.body.avatar,
            user: req.user.id
        }

        //add to comment array
        post.comments.unshift(newComment);

        //save
        post.save().then(post => res.json(post));
    })
    .catch(err => res.status(404).json({postNotFound: 'No post found'}));
});

//route           DELETE api/post/comment/:id/:comment_id
//description     delete comment
//access          private route
router.delete('/comment/:id/:comment_id', passport.authenticate('jwt', {session: false}), (req, res) =>{
    
    Post.findById(req.params.id)
    .then(post => {
      //check the comment 
      if(post.comments.filter(comment => comment._id.toString() === req.params.comment_id).length === 0){
          return res.status(404).json({commentNotFound: 'Comment not found'});
      }

      //remove
      const removeIndex = post.comments
      .map(item => item._id.toString())
      .indexOf(req.params.comment_id);

      //splice 
      post.comments.splice(removeIndex, 1);

      //save
      post.save().then(post => res.json(post));
    })
    .catch(err => res.status(404).json({postNotFound: 'No post found'}));
});

module.exports = router;
